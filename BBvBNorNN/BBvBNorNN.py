import chess.pgn
from tqdm import tqdm
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

#Program control parameters
total_games_analyzed=1000
required_equal_evals = 4
pgn = open("/home/amol/Documents/code/elapsed/bn.pgn")

data = []
pbar = tqdm(total=148204)
while len(data) < total_games_analyzed:
    game = chess.pgn.read_game(pgn)
    if game is None:
        break
    if game.headers["Termination"] != "Normal":
        continue
    node = game.end()
    equal_eval_nodes = 0
    bishops = ""
    while True:
        if node is None:
            break
        fen = node.board().fen().split()[0]
        if any([x in fen for x in ['Q', 'q', 'R', 'r']]):
            node = node.parent
            continue
        if fen.count('B') == 2:
            if ((fen.count('b') == 1 and fen.count('n') == 1) or (fen.count('n') ==2)) and \
               fen.count('p') == fen.count('P'):
                equal_eval_nodes = equal_eval_nodes + 1
                bishops = "White"
        if fen.count('b') == 2:
            if ((fen.count('B') == 1 and fen.count('N') == 1) or (fen.count('N') ==2)) and \
               fen.count('p') == fen.count('P'):
                equal_eval_nodes = equal_eval_nodes + 1
                bishops = "Black"
        if equal_eval_nodes >= required_equal_evals:
            pbar.update(1)
            data.append([game.headers["WhiteElo"],
                         game.headers["BlackElo"],
                         game.headers["TimeControl"],
                         game.headers["Result"],
                         bishops])
            break
        node = node.parent
pbar.close()
df = pd.DataFrame(data, columns=["WhiteElo", "BlackElo", "TimeControl", "Result", "WhoHadBishops"])
df.to_pickle("BN.pkl")
