import chess.pgn
from tqdm import tqdm
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

def time_control_histogram(data, bins=400, img_file="histogram.png", tc="1/0"):
    plt.style.use("seaborn")
    plt.figure(figsize=(6,6))
    plt.hist(data.elapsed, bins=bins, range=(0, bins), histtype='stepfilled')
    plt.title("Total Time Histogram" + "(" + tc + ")")
    plt.ylabel("# of games played", fontsize=12)
    plt.xlabel("Total time elapsed (in seconds)", fontsize=12)
    plt.tight_layout()
    plt.savefig(img_file)

def time_control_pi(data):
    data['tc_str'] = data['time'].astype(str) + "+" + data['increment'].astype(str)
    tc_top_3 = data['tc_str'].value_counts().values[:5]
    others_total = data['tc_str'].value_counts().values[5:].sum()
    tc_top = np.append(tc_top_3, others_total)
    labels = data['tc_str'].value_counts().index[:5].append(pd.Index(['Other']))
    labels_with_pct = [labels[i] + ' (' + "{:.0%}".format(tc_top[i]/np.sum(tc_top)) + ')' for i in range(6)]
    plt.style.use("seaborn")
    plt.figure(figsize=(5,5))
    plt.pie(tc_top, shadow=True, startangle=90, radius=0.9)
    plt.legend(labels_with_pct, fontsize=9)
    plt.title("Games played at time control")
    plt.tight_layout()
    plt.savefig("tc_popularity.png")

def time_control_barh(data):
    plt.style.use("seaborn")
    plt.figure(figsize=(6,6))
    data['tc_str'] = data['time'].astype(str) + "+" + data['increment'].astype(str)
    plt.title("Games played at time control")
    plt.xlabel("# of games played", fontsize=12)
    plt.ylabel("(Seconds)", fontsize=12)
    plt.xticks(fontsize=10)
    plt.yticks(fontsize=10)
    plt.barh(data['tc_str'].value_counts().index[5::-1], data['tc_str'].value_counts().values[5::-1], height=0.8)
#    plt.legend(patches, data['tc_str'].value_counts().index, loc="best")
    plt.tight_layout()
    plt.savefig("tc_popularity.png")
    print("Finished")
#    plt.show()

pgn = open("./db.pgn")
data = []
total_games_analyzed=100000
pbar = tqdm(total=total_games_analyzed)
while len(data) < total_games_analyzed:
    game = chess.pgn.read_game(pgn)
    if game is None:
        break
    try:
        time, increment = [int(i) for i in game.headers["TimeControl"].split("+")]
    except ValueError:
        continue
    gep = game.end().ply()
    if ("rated bullet game" in game.headers["Event"].lower()) and time == 60 and increment == 0:
        try:
            p_1_time_left = game.end().clock()
            p_2_time_left = game.end().parent.clock()
        except AttributeError:
            continue
        if (p_1_time_left is None) or (p_2_time_left is None):
            continue
        if game.headers["Termination"] == 'Time forfeit':
            if p_2_time_left > 3:
                continue
            p_2_time_left = 0
        elapsed = (time - p_1_time_left) + (time - p_2_time_left)
        data.append([time, increment, elapsed])
        pbar.update(1)

df = pd.DataFrame(data, columns=["time", "increment", "elapsed"])
pbar.close()
