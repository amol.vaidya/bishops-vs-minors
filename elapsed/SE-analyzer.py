import glob
import os
import re
import statistics

def seconds(time_str):
    """convert h:mm:ss to total seconds"""
    h, m, s = time_str.split(':')
    return int(h) * 3600 + int(m) * 60 + int(s)

# function to verify each player's clock is monotone
# decreasing (if a player is given time by their
# opponent, then the calculation of total game time
# is not possible given only the PGN data supplied)
def non_increasing(l):
    return all(x>=y for x, y in zip(l, l[1:]))

clock = re.compile(r'(?:\[%clk )(\d:\d\d:\d\d)')

ignore = 0
bullet = 0
time = 0
gameid = ""
alltimes = []
sanetimes = []

statfile = open('stats.txt','w')
statfile.write("%8s %3s %3s %5s %5s\n"
  % ("GameID", "Dur", "nPly", "Wclk", "Bclk"))

for filename in glob.glob('*.pgn'):
  print(filename)
  with open(os.path.join(os.getcwd(), filename), 'r') as f:
    line = f.readline()

    while(line):

      # "Site" is the second line of the each game, so it
      # is a good place to serve up the reset
      if(line.find('[Site ')>=0):
        ignore = 0
        timeout = 0
        bullet = 0
        s = line.split('"')
        s = s[1].split('/')
        gameid = s[-1]

      if(line.find('[Variant')>=0):
        ignore = 1

      if(line.find('[TimeControl')>=0):
        s = line.split('"')
        if(s[1] == '60+0'):
          bullet = 1

      # ignore "Abandoned", "Unterminated", "Rules infraction"
      # and any other nonstandard endings.
      if(line.find('[Termination')>=0):
        s = line.split('"')
        if(s[1] == "Time forfeit"):
          timeout = 1
        elif(s[1] != "Normal"):
          ignore = 1

      if(line[0] == '1'):
        if((bullet == 1) and (ignore == 0)):
          times = clock.findall(line)
          if(len(times) >= 2):
            secs = [ seconds(item) for item in times ]
            p1 = secs[::2]
            p2 = secs[1::2]

            # record the first clock value for each player.
            # it should be 60, but if the player berserked
            # then it will be 30. Whichever, we use this
            # value to determine the elapsed time for that
            # player's clock at the end.
            s1 = p1[0]
            s2 = p2[0]

            if(s1<=60 and s2<=60 and non_increasing(p1)
                and non_increasing(p2)):
              t0 = secs[-1]
              if(timeout == 0):
                t1 = secs[-2]
                dur = s1+s2 - (t0 + t1)
              else:
                dur = s1+s2 - t0

              if(dur >= 0 and dur <=120):
                statfile.write("%8s %3d %3d %5d %5d\n"
                  % (gameid, dur, len(secs), s1, s2))

                alltimes.append(dur)
                if(s1==60 and s2==60):
                  sanetimes.append(dur)

      line = f.readline()

f.close()
statfile.close()

print("Number of games: ", len(alltimes))
print("Average time: %s seconds".format(statistics.mean(alltimes)))
print("Median time: %s seconds".format( statistics.median(alltimes)))

print("Number of non-berserked games: ", len(sanetimes))
print("Average time: %s seconds".format( statistics.mean(sanetimes)))
print("Median time: %s seconds".format(statistics.median(sanetimes)))
